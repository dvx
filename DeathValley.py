#!/usr/bin/env python
"""
Death Valley X

(C) 2008 Gergely Imreh <imrehg@gmail.com>
--- using:
    Accelerometer code: Martin Senkerik <martinsenkerik@gmail.com>
    (BSD licence)

GPLv3 or later
"""

import pygtk
pygtk.require('2.0')
import gtk, gobject
import cairo
import pango
import random
import threading, time
import accelero
from math import pi, ceil

############

# Accelerometer thread
amthread = None

# Lots of output
debug = False

__version__ = '0.1'

############



def main_quit(obj):
    ## Shut down accelerometer thread when finished
    amthread.bRun = False

def progress_timeout(object):
    """ Repeated event to redraw graphics """
    ## Area to redraw
    x, y, w, h = object.allocation
    object.window.invalidate_rect((0,0,w,h),False)
    ## Every segment survived worth 1 point
    object.score = object.score+1
    ## If crashed, game over
    return object.checkcrash()

class Field(gtk.DrawingArea):

    def __init__(self):
        gtk.DrawingArea.__init__(self)
        self.add_events(gtk.gdk.BUTTON_PRESS_MASK |
                        gtk.gdk.BUTTON_RELEASE_MASK |
                        gtk.gdk.BUTTON1_MOTION_MASK |
                        gtk.gdk.SCROLL_MASK
                        )
	## Implement later: Pause on touch of screen
	# self.connect("button_press_event", self.on_button_press_event)
	
	
        self.connect("expose_event", self.do_expose_event)
	 ## Timer to redraw screen:  every 170ms
	 ## Right now: go faster -> slugish graphics, go slower -> slugish everything
        self.timer = gobject.timeout_add (170, progress_timeout, self)

	 ## Starting path
	self.path = [200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200]
	self.pwidth = [180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180]
	 ## Max path segments
	self.maxindex = 12

	 ## Probabilities of road direction changes
	 ## 0,1 : probability of UP,AHEAD, when going UP now
	 ## 2,3 : probability of UP,AHEAD, when going AHEAD now
	 ## 4,5 : probability of UP,AHEAD, when going DOWN now
	 ## (third probability is always 1-p0-p1 for them to add up to 1)
	self.probs = [0.5, 0.4, 0.2, 0.6, 0.2, 0.4]
	 ## Probabilities of width changes:
	 ## 0,1 : getting NARROWER or STAYING SAME.
	 ## probability of getting WIDER is just 1-p0-p1 
	self.wprobs = [0.1, 0.8]

	 ## Starting by going ahead
    	self.pstate = 0
	 ## Unit of changes in road segment UP or DOWN in any given step
        self.stepsize = 15
	 ## Units of change in width
	self.wstep = 10
	 ## Defines the ship's speed: max step in one redraw either up or down.
	self.shipspeed = 30
	 ## Starting shipposition
	self.shippos = 290	
	 ## Start going ahead
	 ## Ratio is how much the speed changes compared to the .shipspeed
	self.shipratio = 0;
	 ## We are healthy first
        self.crash = False
	 ## Reset score
	self.score = 0

     
    def do_expose_event(self, w,  event):
       # Handle the expose-event by drawing
       try:
            self.cr = self.window.cairo_create()
       except AttributeError:
            #return self._expose_gdk(event)
            raise
       return self._expose_cairo(event)


    def _expose_cairo(self, event):
	""" What to do on expose """
        # Create the cairo context
        self.cr = self.window.cairo_create()

        # Restrict Cairo to the exposed area; avoid extra work
        self.cr.rectangle(event.area.x, event.area.y,
                event.area.width, event.area.height)
        self.cr.clip()
	## Drawing background
	self.redrawfield()
	## Draw ship
	self.drawship()


    def refresh(self):
	""" Refresh screen """
        print "do_refresh"
        x, y, w, h = self.allocation
        self.window.invalidate_rect((0,0,w,h),False)

    def drawship(self):
	""" Call ship position update and draw ship - in pieces of crashed """
	## New position
	self.updateshippos()
	if debug: print "DEBUG:","Draw ship at ",self.shippos
	## Draw ship in pieces (with a little randomness) if crashed
	if self.crash:
	   self.cr.set_source_rgba(1, 0, 0, 0.6)
	   self.cr.arc(20+10*random.random(), self.shippos+10*random.random(), 3.0, 0, 2*pi)
	   self.cr.fill()
	   self.cr.arc(20+10*random.random(), self.shippos-10*random.random(), 4.0, 0, 2*pi)
	   self.cr.fill()
	   self.cr.arc(20-10*random.random(), self.shippos-10*random.random(), 5.0, 0, 2*pi)
	   self.cr.fill()
	   self.cr.arc(20-10*random.random(), self.shippos+10*random.random(), 5.0, 0, 2*pi)
	   self.cr.fill()
        else:
	   ## Draw ship in one piece still, yippie....
	   self.cr.set_source_rgba(1, 0.8, 0.2, 0.6)
	   self.cr.arc(20, self.shippos, 10.0, 0, 2*pi)
	   self.cr.fill()

    def checkcrash(self):
	""" Routine to check every update, whether the ship crashed and to vibration if it did """
	## Check for crash - taking ship size of r=5 into account
	if (self.shippos <= self.path[0]+5 ) or (self.shippos >= self.path[0]+self.pwidth[0]-5):
		if debug: print "DEBUG:","Crashed!!"
		## Good vibrations!
		intensity = 255
                outfile = open("/sys/class/leds/neo1973:vibrator/brightness", "w", 1)
                outfile.write("%s\n" % str(intensity))
                time.sleep(0.5)
                outfile.write("0\n")
                outfile.close()
		self.crash = True
		return False
	else:
		## No crash, carry on
		return True
	


    def updateshippos(self):
	""" Updating the ship position, according to tilt~ship-speed """
	self.shippos = self.shippos+self.shipspeed*self.shipratio
	if debug: print "DEBUG:","Updateship: Pos:",self.shippos,"Speed:",self.shipratio,"New Pos:",self.shippos


    def redrawfield(self):
	""" Drawing background, score, valley walls """
	## Get window size
	width, height = self.window.get_size() 
        ## Fill the background with gray
        self.cr.set_source_rgb(0, 0, 0)
        self.cr.rectangle(0, 0, width, height)
        self.cr.fill()

	## Display score: rotated, in corner
        self.cr.set_source_rgb(1, 0.0, 0.0)
        self.scorefield = self.create_pango_layout(str(self.score))
	self.scorefield.set_font_description(pango.FontDescription("sans serif 10"))
	self.cr.move_to(440,30)
        self.cr.rotate( 90 / (180.0 / pi));
	self.cr.update_layout(self.scorefield)
        self.cr.show_layout(self.scorefield)
        self.cr.rotate( -90 / (180.0 / pi));

	########## Drawing lines##########

	## Change probabilities: close to edges, too wide or too narrow paths
	inprob,inwprob = self.changeprob(self.path[-1],self.pwidth[-1],self.probs,self.wprobs)
	## Where should the road go next?
	self.pstate = self.newstate(self.pstate,inprob)


	## Calculate position of next road segment
	newpos = self._newpath(self.path,self.pstate)
	if debug: print "DEBUG:","New Road segment:",newpos
        self.path.append(newpos)

	## Calculate new width
	wy = self._newwidth(self.pwidth[-1],inwprob)
	if debug: print "DEBUG:","New Width       :",wy
	self.pwidth.append(wy)

	## Set color of valley walls
        self.cr.set_source_rgb(0.9, 0.9, 0.9)

	## Index of staring positions
	to = 0
	## length of elementary segments
	tos = ceil(width/self.maxindex)
	## Loop through segments
        for index, item in enumerate(self.path):
		if index < self.maxindex:
	            self.cr.move_to(to,item)
                    to += tos
		
		    ## Curves to sloooow, using line_to only....
		    #### self.cr.curve_to(to,item,to-tos,self.path[index+1],to,self.path[index+1])
	            #### self.cr.move_to(to-tos,item+self.pwidth[index])
		    #### self.cr.curve_to(to,item+self.pwidth[index],to-tos,self.path[index+1]+self.pwidth[index+1],to,self.path[index+1]+self.pwidth[index+1])

		    ## Drawing lines
		    # Upper line
                    self.cr.line_to(to,self.path[index+1])
	            self.cr.move_to(to-tos,item+self.pwidth[index])
		    # Lower line
                    self.cr.line_to(to,self.path[index+1]+self.pwidth[index+1])

	## Draw on screen
        self.cr.stroke()

	## Throw away used path segments
	self.path.pop(0)
	self.pwidth.pop(0)

	##### End of redraw()	



    def _newwidth(self,width,wprob):
	""" Use probabilities to change path width """
	change = random.random()
	if change < wprob[0]:
		## Narrower
		return width-self.wstep
	elif change < wprob[0]+wprob[1]:
		## Stay the same
		return width
	else:
		## Wider
		return width+self.wstep


    def _newpath(self,path,pstate):
	""" Change path direction according to status """
	if pstate == -1:
		## Move road up (compared to the last piece of road already generated)
		return path[-1]-self.stepsize
	elif pstate == 0:
		## Keep ahead
		return path[-1]
        else:
		## Move road down
        	return path[-1]+self.stepsize
        


    def newstate(self,state,probs):
	""" Use Markov probabilities to change direction of path """
	## Now: Going up
	if state == -1:
	    sprob = probs[0:2]
	## Now: Going straight 
	elif state == 0:
	    sprob = probs[2:4]
	## Now: Going down
	else:
	    sprob = probs[4:6]

	## Choose new direction
	change = random.random()
	if change < sprob[0]:
		## Go up
		return -1
	elif change < sprob[0]+sprob[1]:
		## Stay the same
		return 0
	else:
		## Go down
		return 1
			
    def changeprob(self,pos,width,probs,wprobs):
	""" Change probabilities of directions of winding path """
	if debug: print "DEBUG:","Before next: Pos:",pos,"Width:",width
	outprobs = probs[:]
	outwprobs = wprobs[:]

	## Path direction
	if pos <= 10:
	    if debug: print "DEBUG:","Too HIGH"
	    outprobs[0] = 0
	    outprobs[2] = 0
	elif pos >= 470-width:
	    if debug: print "DEBUG:","Too LOW"
            norm = outprobs[2]+outprobs[3]
	    outprobs[2] = outprobs[2]/norm
	    outprobs[3] = outprobs[3]/norm
            norm = outprobs[4]+outprobs[5]
	    outprobs[4] = outprobs[4]/norm
	    outprobs[5] = outprobs[5]/norm
	## Path width: min 70, max 180
	if width <= 70:
	    if debug: print "DEBUG:","Too Narrow"
	    outwprobs[0] = 0
	elif width >= 180:
	    if debug: print "DEBUG:","Too Wide"
            norm = outwprobs[0]+outwprobs[1]
	    outwprobs[0] = outwprobs[0]/norm
	    outwprobs[1] = outwprobs[1]/norm

	return outprobs,outwprobs


class Form:
     """ Form to display game area """

     ## accelerometer thread
     t = None
     gtk.gdk.threads_init()

     def __init__(self):
	""" Draw window: buttons and drawing arrea """
        self.ready_semaphore = threading.Semaphore()
        self.ready_semaphore.acquire()

	## Create window
	self.window = gtk.Window()
        self.window.connect("delete-event", main_quit)
        self.window.connect('destroy', main_quit)
 	self.window.set_title("DeathValleyX")


	## vertical box to align buttons and drawing screen
	## input Homogeneous, Spacing
	box1 = gtk.VBox(False,0)
        
	## exit button
	button = gtk.Button("eXit")
        button.connect_object("clicked", main_quit, self.window)
	box1.pack_start(button,True,True,0)
	button.show()

	## Drawing area
        self.drawing_area = Field()
        self.drawing_area.set_size_request(480, 480)
        box1.pack_start(self.drawing_area, False, False, 0)
        self.drawing_area.show()

	## Level-down and level-up buttons
	## All in horizontal box
	box2 = gtk.HBox(False,0)
	button = gtk.Button("|")
	# DOES NOT WORK YET
	button.Sensitive = False
	#### button.connect_object("clicked", main_quit, self.window)
	box2.pack_start(button,True,True,0)
	button.show()
	#
	button = gtk.Button("+")
	#DOES NOT WORK YET
	#### button.connect_object("clicked", main_quit, self.window)
	box2.pack_start(button,True,True,0)
	button.show()

	box2.show()
	box1.pack_start(box2,True,True,0)

	self.window.add(box1)
	box1.show()
	self.window.show()
	## Finish drawing window

	## start accelerometer thread
        self.ready_semaphore.release()
  


def main():
    """ Main function - create window and accelerometer thread """
    ## Accelerometer thread
    global amthread

    ## Draw windows
    form = Form()

    ## start accelerometer
    amthread = accelero.Worker(form)
    amthread.start()
    
    gtk.main()

    ## when exit
    return 0



if __name__ == "__main__":
    main()

