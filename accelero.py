#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
in: Death Valley X

    Accelerometer code: Martin Senkerik <martinsenkerik@gmail.com>
    (BSD licence)

"""

import struct
import sys
from math import sqrt
import threading, time, gtk
from math import *


debug = False

firstsensorfile = "/dev/input/event3"

#int, int, short, short, int
fmt = 'iihhi'

#open file in binary mode
accel1 = open(firstsensorfile,"rb")

def sign(val):
    try:
        return abs(val) / val
    except ZeroDivisionError:
        return 1
  
class Worker(threading.Thread):
    form = None
    bRun = True
    def __init__(self, form):
	if debug: print "DEBU:","Accelerometer thread initialized"
        self.form = form
        self.x = axis()
        self.y = axis()
        threading.Thread.__init__ ( self, name="Worker" )
        
    def run(self):
        event = accel1.read(16)

	if debug: "DEBUG:","Accelerometer thread running"
        self.form.ready_semaphore.acquire()

	## Repeat while not received signal to stop (through bRun)
        while event and self.bRun:
            
            (time1, time2, type, code, value) = struct.unpack(fmt,event)
	    ## Only reading X, Y now
            if type == 2:
                if code == 0:
                    self.x.low_pass(time1, time2, value)
                    self.y.low_pass(time1, time2)
                    #self.z.low_pass(time1, time2)
                if code == 1:
                    self.x.low_pass(time1, time2)
                    self.y.low_pass(time1, time2, value)
                    #self.z.low_pass(time1, time2)
                if code == 2:
                    #self.x.low_pass(time1, time2)
                    #self.y.low_pass(time1, time2)
                    #self.z.low_pass(time1, time2, value)
                    pass
            if type == 0 and code == 0:            
		 ## When new values read, change ship speed in game
		 self.form.drawing_area.shipratio = self.y.val/-1000.0
            event = accel1.read(16)
    
	## If finished, clean up
        accel1.close()
        gtk.main_quit()
    
    def calibrate(self):
	""" Calibrate if sensor readings are not centered where they ought to """
        self.x.calib = self.x.val
        self.y.calib = self.y.val


class axis:
    last_val_lp = 0
    old_time1 = 0
    old_time2 = 0
    RC = 5
    val_lp = 0
    val = 0
    calib = 0
    def _get_delta(self, time1, time2):
        new_time1 = long(time1) - long(self.old_time1)
        new_time2 = long(time2) - long(self.old_time2)

        if new_time2 < 0.0:
            new_time1 -= 1
            new_time2 += 1000000

        self.old_time1 = time1
        self.old_time2 = time2
        
        return float("%s.%s" % (str(new_time1), str(new_time2)))

    def low_pass(self, time1, time2, value = None):
        
        # currently bypassed
        #---------------------------
        
        if value == None:
            value = self.val - self.calib
        else:
            self.val = value
            value = value - self.calib
         
        #dt = self._get_delta(time1, time2)
        #alpha = dt / float(self.RC + dt)
        
        #self.val_lp = self.last_val_lp + alpha * (value - self.last_val_lp)
        self.val_lp = value
        
        #self.last_val_lp = self.val_lp
    
        
    def get_angle(self):
        return (float(self.val_lp) / 1000) * (pi/2)
    
    def __repr__(self):
        return "<axis %s (%s)>" % (str(int(self.val_lp)), str(self.val))

    
